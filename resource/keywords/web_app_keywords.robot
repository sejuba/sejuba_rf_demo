*** Settings ***

Documentation   Importing web and robotframework built-in keywords.
Library  SeleniumLibrary   timeout=10
Library    Collections
Library    OperatingSystem
Library    String
Library    DateTime
Library  ./custom.py
Variables         ../../resource/settings/const.py
Resource        ../../resource/settings/page_element_variables.robot

*** Variables ***
${BROWSER}        chrome
${DELAY}          0.5
${TIME OUT}          15
${MIN WAIT}    2

${HUB}    http://localhost:4444/wd/hub

*** Keywords ***
open chrome browser
    [Documentation]
    Set Suite Variable    ${BROWSER}        chrome
    ${desired_caps} =   create dictionary  enableVNC=${True}
    Open Browser    ${PAGE_URL}    ${BROWSER}  None    ${HUB}    desired_capabilities=${desired_caps}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

open browser to landing page
    [Documentation]
    Run Keyword If      '${MODE}' == 'live'
     ...     Run Keywords
     ...     open chrome browser
     ...     AND    Set Selenium Speed    ${DELAY}
     ...     ELSE IF     '${MODE}' == 'HeadlessFirefox'     Firefox true headless
     ...     ELSE IF     '${MODE}' == 'HeadlessChrome'     Open Headless Chrome Browser to Page

Open Headless Chrome Browser to Page
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-web-security
    Call Method    ${chrome_options}    add_argument    --allow-file-access-from-files
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    ${PAGE URL}

Open Headless firefox Browser to Page
    ${firefox options}=     Evaluate    sys.modules['selenium.webdriver'].firefox.webdriver.Options()    sys, selenium.webdriver
    Call Method    ${firefox options}   add_argument    -headless
    Create Webdriver    Firefox    firefox_options=${firefox options}
    Set Window Size    1920    1080
    Go To    ${PAGE URL}

say hello ${message}
    say_hello  ${message}
    Set Suite Variable    ${CUSTOM}  ${message}
    log to console  ${CUSTOM}

verify that exception is rendered for anonymous user
    wait until page contains  ${signup message}

verify user can successfully signup
    ${random prefix}=  Generate Random String  length=4
    wait until page contains element  ${sigup link}
    click element  ${sigup link}
    wait until page contains element  ${first name input field}
    input text  ${first name input field}  ${random prefix}-firstname
    input text  ${last name input field}  ${random prefix}-lastname
    input text  ${user name input field}  ${random prefix}-firstname
    input text  ${user email input field}  ${random prefix}@gmail.com
    input text  ${user password input field}  password121
    input text  ${user confirm password input field}  password121
    click element  ${signup button}
#    wait until page contains element  ${login link}
#    click element  ${login link}
#    wait until page contains  ${user name input field}



