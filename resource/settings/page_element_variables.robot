*** Variables ***
#ELEMENT

${search input field}         css:#app > div > div > section > div > div > div.col-md-6 > div > input
${start button}         css:#app > div > div > section > div > div > div.col-md-4 > button
${nav bar}   css:#app > div > div > header > nav > div > button > span
${sigup link}    css:ul.navbar-nav.align-items-lg-center.ml-lg-auto > li:nth-child(2) > a > span.nav-link-inner--text
${first name input field}    css:input[placeholder="First name"]
${last name input field}    css:input[placeholder="Last name"]
${user name input field}    css:input[placeholder="Username"]
${user email input field}    css:input[placeholder="Email"]
${user password input field}    css:input[placeholder="Password"]
${user confirm password input field}    css:input[placeholder="Confirm password"]
${signup button}    xpath://*[@class="btn my-4 btn-primary" and contains(., "Sign Up")]

# signup exception messages
${signup message}         Please Signup or Login to download the video.
${login link}  css:ul.navbar-nav.align-items-lg-center.ml-lg-auto > li:nth-child(1) > a > span.nav-link-inner--text
