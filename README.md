# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* `git clone git@bitbucket.org:sejuba/sejuba_rf_demo.git`
* `pip install -r requirements.txt`

### Chrome driver ###
* Download the latest version of [chrome from here](https://chromedriver.storage.googleapis.com/81.0.4044.69/chromedriver_win32.zip)
* Unzip the content, for windows it should be a chromedriver.exe file
* Copy the chromedriver to PYTHON_HOME/Scripts
* If everything is ok you should be able to run the framework and launch chrome

### How do I run tests? ###
pass possible mode to the test MODE:
* live -- launch live chrome browser
* HeadlessFirefox -- headless firefox
* HeadlessChrome -- headless chrome

* `robot -v MODE:'live' test/web/template.robot`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Useful links ###
* https://robotframework.org/
* https://robotframework.org/robotframework/
* https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html

### Recommended medium posts ###
* https://medium.com/faun/automated-testing-setup-with-robotframework-and-gitlab-ci-5bd01cf26141
* https://medium.com/faun/browsers-in-containers-with-selenoid-and-test-automation-using-robot-framework-selenium-part-1-b0d5032daaa7
* https://medium.com/@sejuba/browsers-in-containers-executing-robot-framework-tests-with-selenoid-part-2-2ca76059d24e



### Who do I talk to? ###

* sejuba@gmail.com
