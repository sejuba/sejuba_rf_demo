*** Settings ***
Documentation     Test suite example to quickly create robotframework test cases
...               to quickly create robotframework test cases

Resource          ${EXECDIR}/resource/keywords/web_app_keywords.robot
Suite Setup    open browser to landing page
#Suite Teardown    Close Browser

*** Variables ***
${message}  hello from custom library

*** Test Cases ***
scenario: successful sign in & sign out of the application
    [Documentation]    add your test case description here
    ...               add another test case description here
    ...               and add another test case description here
    [Tags]    smoke
    verify that user can search for a video
    verify that exception is rendered for anonymous user
    verify user can successfully signup

say hello from custom library
    say hello ${message}

*** Keywords ***
verify that user can search for a video
    wait until page contains element  ${search input field}
    Input Text  ${search input field}  https://youtu.be/7YgTEm_-i4U
    click element  ${start button}


